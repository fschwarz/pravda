# Pravda

This tool provides exercices in formal logic, more precisely for proof systems.

## Building the tool
Be sure to have installed tsc, the TypeScript compiler.
Run tsc in the root folder.
The release version is then in the folder dist.